
To use this program, please use python 3.6.5. 
Please execute it from the CLI, ~/project_path/$ python mirrors.py.
Don't forget to allow it execution rights ~/project_path/$ python $ chmod +x mirrors.py.

This program is not optimized but still quite efficient, below are thoughts about the memory usage and the cpu charge.

Memory usage :
R*C*1 char size in bytes 
2 laser objects : 4 char, 5 int each

CPU cost :
Worst case scenario, 2*R*C table roaming with 2 if levels