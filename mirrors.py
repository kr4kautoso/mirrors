#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

"""
We can get the arguments of the cli parsed to int using this :
problemDatas=[int(row)for row in sys.argv]
but giving multiline arguments can be tricky, so i created a function allowing the user to enter those arguments
after running the script.
this function supports multi-line input.
"""


def readfunction():
    lines = []
    print("Please enter the problem's parameters :")
    while True:
        line = raw_input()
        if line:
            lines.append(line.split(" "))
        else:
            break

    ret = [[int(column) for column in row] for row in lines]

    return ret


def printTab():
    # print the tab
    for r in tab:
        for c in r:
            print(c, " ")
        print()
    print()
    print()


def direction(x):  # ↓ : X ----- → : Y
    return {
        "north": [-1, 0],
        "south": [1, 0],
        "east": [0, 1],
        "west": [0, -1]
    }.get(x, [0, 0])


def printedDirection(x):
    return {
        "north": "↑",
        "south": "↓",
        "east": "→",
        "west": "←"
    }.get(x, "?")


problemDatas = readfunction()
r = problemDatas[0][0]
c = problemDatas[0][1]
m = problemDatas[0][2]
n = problemDatas[0][3]
tab = []

# exit cases :
if (r == 0 or c == 0):
    print("Impossible")
    exit(0)
if (m == 0 and n == 0):
    print("Impossible")
    exit(0)

try:
    # init tab
    for i in range(r):
        tab.append([])
        for j in range(c):
            tab[i].append(".")

    # adding / with M
    for i in range(m):
        tab[problemDatas[i + 1][0] - 1][problemDatas[i + 1][1] - 1] = "/"  # -1 bcz [1,1] is [0,0] on the tab
    # adding \ with N
    for i in range(n):
        tab[problemDatas[i + m + 1][0] - 1][problemDatas[i + m + 1][1] - 1] = "\\"  # -1 bcz [1,1] is [0,0] on the tab
except IndexError:
    print("The problems parameters are wrong. Please relaunch the program.")
    exit()

# laser obj
class laser():
    direction = "east"
    posx = 0
    posy = 0
    nbsolution = 0
    resPosx = 0
    resPosy = 0

    def nextCase(self):
        if tab[self.posx][self.posy] == ".":  # case is free
            tab[self.posx][self.posy] = printedDirection(self.direction)
            self.posx += direction(self.direction)[0]
            self.posy += direction(self.direction)[1]
        elif tab[self.posx][self.posy] == "/":  # case is /

            if self.direction == "north":
                self.direction = "east"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

            elif self.direction == "east":
                self.direction = "north"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

            elif self.direction == "south":
                self.direction = "west"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

            elif self.direction == "west":
                self.direction = "south"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

        elif tab[self.posx][self.posy] == "\\":  # case is \

            if self.direction == "north":
                self.direction = "west"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

            elif self.direction == "east":
                self.direction = "south"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

            elif self.direction == "south":
                self.direction = "east"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

            elif self.direction == "west":
                self.direction = "north"
                self.posx += direction(self.direction)[0]
                self.posy += direction(self.direction)[1]

        elif tab[self.posx][self.posy] in ["↑", "↓", "→", "←"]:
            self.nbsolution += 1

            self.resPosx = self.posy
            self.resPosy = self.posx
            self.posx += direction(self.direction)[0]
            self.posy += direction(self.direction)[1]

            # print([self.nbsolution,self.resPosx,self.resPosy])
            # printTab()

            # print("Found at ", self.posx, "/", self.posy, "direction : ", printedDirection(self.direction), " against  ",
            #       tab[self.posx-1][self.posy-1])

            if self.direction == "north":
                if tab[self.posx][self.posy] == "→":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx
                if tab[self.posx][self.posy] == "←":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx

            elif self.direction == "east":
                if tab[self.posx][self.posy] == "↑":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx
                if tab[self.posx][self.posy] == "↓":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx

            elif self.direction == "south":
                if tab[self.posx][self.posy] == "→":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx
                if tab[self.posx][self.posy] == "←":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx

            elif self.direction == "west":
                if tab[self.posx][self.posy] == "↑":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx
                if tab[self.posx][self.posy] == "↓":
                    self.nbsolution += 1
                    self.resPosx = self.posy
                    self.resPosy = self.posx

            else:
                return "", self.posx, " ", self.posy, " ", 0  # no mirror needed

            self.posx += direction(self.direction)[0]
            self.posy += direction(self.direction)[1]

        if(lzr.posx == 0 and lzr.posy == -1):
            self.nbsolution = 1
        return [self.nbsolution, self.resPosx, self.resPosy]


lzr = laser()
# reverse lazer path
lzr.posx = tab.__len__() - 1
lzr.posy = tab[0].__len__() - 1
lzr.direction = "west"
while (lzr.posx < tab.__len__() and lzr.posy < tab[0].__len__() and lzr.posx >= 0 and lzr.posy >= 0):
    x = laser.nextCase(lzr)
    #printTab()
if(x[0] == 1):
    print (0)

else:
    lzr = laser()
    # laser path
    while (lzr.posx < tab.__len__() and lzr.posy < tab[0].__len__() and lzr.posx >= 0 and lzr.posy >= 0):
        x = laser.nextCase(lzr)
        #printTab()

    if (x[0] == 0):
        print("Impossible")
        exit(0)

    print(x[0], " ", x[1], " ", x[2])
